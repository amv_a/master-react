import React from 'react';
/**
 * This functional component will display
 * the amount of counters the app has available
 * with a count bigger than 0.
 * @param {number} totalCounters: the amount
 * of available counters.
 */
const NavBar = ({ totalCounters }) => {
	return (
		<nav className='navbar navbar-light bg-light'>
			<a className='navbar-brand' href='#'>
				Navbar{''}
				<span className='badge badge-pill badge-secondary m-2'>
					{totalCounters}
				</span>
			</a>
		</nav>
	);
};

export default NavBar;
