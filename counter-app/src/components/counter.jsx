import React, { Component } from 'react';
/**
 * This is the class that holds
 * the structure of a counter
 */
class Counter extends Component {
	/**
	 * This method will add
	 * one class or another if the
	 * count equals 0 or is bigger
	 * than 0.
	 */
	getBadgeClasses() {
		let classes = 'col-1 badge m-2 badge-';
		classes += this.props.counter.value === 0 ? 'warning' : 'primary';
		return classes;
	}
	/**
	 * This method will change the format
	 * of the count, if the count is 0,
	 * it will display "Zero" if not,
	 * it will display the corresponding
	 * value.
	 */
	formatCount() {
		const { value } = this.props.counter;
		return value === 0 ? 'Zero' : value;
	}
	/**
	 * This method is responsible for
	 * disabling the decrement button
	 * when the count reaches 0.
	 */
	isDisabled() {
		const { value } = this.props.counter;
		return value === 0 ? true : false;
	}
	render() {
		const { onIncrement, onDelete, counter, onDecrement } = this.props;
		return (
			<React.Fragment>
				<span className={this.getBadgeClasses()}>{this.formatCount()}</span>
				<button
					onClick={() => onIncrement(counter)}
					className='btn btn-secondary btn-sm m-2 col-1'
				>
					+
				</button>
				<button
					onClick={() => onDecrement(counter)}
					className='btn btn-secondary btn-sm m-2 col-1'
					disabled={this.isDisabled()}
				>
					-
				</button>
				<button
					onClick={() => onDelete(counter.id)}
					className='btn btn-danger btn-sm m-2 col-1 '
				>
					X
				</button>
				<br></br>
			</React.Fragment>
		);
	}
}

export default Counter;
