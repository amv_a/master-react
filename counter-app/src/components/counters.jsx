import React, { Component } from 'react';
import Counter from './counter';
/**
 * This component hold all counters
 * interface.
 * The counters on themselves as well
 * as the reset button.
 */
class Counters extends Component {
	render() {
		const {
			counters,
			onReset,
			onIncrement,
			onDelete,
			onDecrement,
		} = this.props;
		return (
			<div>
				<button className='btn btn-primary btn-sm m-2' onClick={onReset}>
					Reset
				</button>
				<br></br>
				{counters.map((counter) => (
					<Counter
						key={counter.id}
						//We pass the whole object
						counter={counter}
						onDelete={onDelete}
						onIncrement={onIncrement}
						onDecrement={onDecrement}
					></Counter>
				))}
			</div>
		);
	}
}

export default Counters;
