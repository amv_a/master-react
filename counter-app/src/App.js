import React, { Component } from 'react';
import NavBar from './components/NavBar';
import './App.css';
import Counters from './components/counters.jsx';

/**
 * Foundational class for the aplication, it holds the
 * event handlers as well ass the complete state of the
 * aplication.
 */
class App extends Component {
	state = {
		counters: [
			{ id: 1, value: 4 },
			{ id: 2, value: 0 },
			{ id: 3, value: 3 },
			{ id: 4, value: 1 },
		],
	};
	/**
	 * This method handles the onClick done
	 * on the delete button, it will delete
	 * a counter component when clicked.
	 * @param String: The id of the element
	 * to be removed.
	 */
	handleDelete = (counterId) => {
		//These handlers use arrow functions to avoid biding.
		const counters = this.state.counters.filter((c) => c.id !== counterId);
		this.setState({ counters });
	};
	/**
	 * This method handles the onClick done
	 * on the reset button, it will put
	 * all counters to 0.
	 */
	handleReset = () => {
		const counters = this.state.counters.map((c) => {
			c.value = 0;
			return c;
		});
		this.setState({ counters });
	};
	/**
	 * This method handles the onClick done
	 * on the "+" button, it will change
	 * the state adding 1 to the count.
	 * @param counter: The counter element that
	 * has recieved the event.
	 */
	handleIncrement = (counter) => {
		const counters = [...this.state.counters];
		const index = counters.indexOf(counter);
		counters[index] = { ...counter };
		counters[index].value++;
		this.setState({ counters });
	};
	/**
	 * This method handles the onClick done
	 * on the "+" button, it will change
	 * the state adding 1 to the count.
	 * @param counter: The counter element that
	 * has recieved the event.
	 */
	handleDecrement = (counter) => {
		const counters = [...this.state.counters];
		const index = counters.indexOf(counter);
		counters[index] = { ...counter };
		counters[index].value--;
		this.setState({ counters });
	};
	render() {
		return (
			<React.Fragment>
				<NavBar
					totalCounters={this.state.counters.filter((c) => c.value > 0).length}
				></NavBar>
				<main className='container'>
					<Counters
						counters={this.state.counters}
						onReset={this.handleReset}
						onDelete={this.handleDelete}
						onIncrement={this.handleIncrement}
						onDecrement={this.handleDecrement}
					></Counters>
				</main>
			</React.Fragment>
		);
	}
}

export default App;
