import _ from 'lodash';

export default function paginate(items, pageNumber, pageSize) {
	const startIndex = (pageNumber - 1) * pageSize;
	/**
	 * This is done because in order to use lodash functions
	 * in chain we have to transform the array using
	 * a lodash wrapper.
	 * After that we chaing slice, that cuts the array in parts
	 * starting on a given index, and take will create the new
	 * array from the point the slice gives it.
	 * Finally value transforms back into a regular array.
	 */
	return _(items).slice(startIndex).take(pageSize).value();
}
