import React from 'react';
import Form from './common/form';
import Joi from 'joi-browser';
/**
 * Class form the LoginForm component, it will hold
 * the data of the logged user (for now) as
 * well as display any validation issues.
 */
class LoginForm extends Form {
	state = {
		data: {
			username: '',
			password: '',
		},
		errors: {},
	};
	/**
	 * This is an object for the libary Joi, it
	 * will make validating a form easier.
	 */

	schema = {
		username: Joi.string().required().label('Username'),
		password: Joi.string().required().label('Password'),
	};

	doSubmit = () => {
		//Call to the server.
		console.log('Submited');
	};
	render() {
		return (
			<div>
				<h1>Login</h1>
				<form className='mt-3' onSubmit={this.handleSubmit}>
					{this.renderInput('username', 'Username')}
					{this.renderInput('password', 'Password', 'password')}
					{this.renderButton('Login')}
				</form>
			</div>
		);
	}
}

export default LoginForm;
