import React from 'react';
/**
 * Functional component for the
 * Not Found component.
 */
const NotFound = () => {
	return <h1>Not Found</h1>;
};
export default NotFound;
