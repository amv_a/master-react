import React from 'react';
import Form from './common/form';
import Joi from 'joi-browser';

class RegisterForm extends Form {
	state = {
		data: {
			email: '',
			password: '',
			name: '',
		},
		errors: {},
	};
	schema = {
		email: Joi.string().required().label('Email').email(),
		password: Joi.string().required().label('Password').min(5),
		name: Joi.string().required().label('Name'),
	};
	render() {
		return (
			<div>
				<h1>Register</h1>
				<form className='mt-3' onSubmit={this.handleSubmit}>
					{this.renderInput('email', 'Email')}
					{this.renderInput('password', 'Password', 'password')}
					{this.renderInput('name', 'Name')}
					{this.renderButton('Register')}
				</form>
			</div>
		);
	}
}

export default RegisterForm;
