import React from 'react';
/**
 * Functional component for the
 * Customers component.
 */
const Customers = () => {
	return <h1>Customers</h1>;
};
export default Customers;
