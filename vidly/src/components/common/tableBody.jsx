import React, { Component } from 'react';
import _ from 'lodash';
/**
 * This custom component handles the body
 * part of a table.
 * @param {Array} data
 * @param {Array} columns
 * @param {function} onDelete
 * @param {function} onLike
 */
class TableBody extends Component {
	/**
	 * A method that renders the content of
	 * a cell.
	 * @param {Object} item: Contains the data of
	 * the elements that will be inserted inside
	 * the table.
	 * @param {Object} column: Contains the data
	 * that will identify the column as well as
	 * any nested elements or components
	 */
	renderCell = (item, column) => {
		if (column.content) return column.content(item);

		return _.get(item, column.path);
	};
	/**
	 * A method that will asing a valid identifying
	 * key to the cell element.
	 * @param {Object} item: Contains the data of
	 * the elements that will be inserted inside
	 * the table.
	 * @param {Object} column: Contains the data
	 * that will identify the column.
	 */
	createKey(item, column) {
		return item._id + (column.path || column.key);
	}
	render() {
		const { data, columns } = this.props;
		return (
			<tbody>
				{data.map((item) => (
					<tr key={item._id}>
						{columns.map((column) => (
							<td key={this.createKey(item, column)}>
								{this.renderCell(item, column)}
							</td>
						))}
					</tr>
				))}
			</tbody>
		);
	}
}

export default TableBody;
