import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';

/**
 * This is a functional component that
 * handles the pagination of our tables.
 * @param {number} itemsCount: Amount of items
 * available.
 * @param {number} pageSize: Amount of items
 * on each page.
 * @param {number} currentPage: Number of the current
 * page.
 * @param {function} onPageChange: Handler for the
 * changing page event.
 */
const Pagination = ({ itemsCount, pageSize, currentPage, onPageChange }) => {
	const pagesCount = Math.ceil(itemsCount / pageSize);
	if (pagesCount === 1) return null;
	/**
	 * We add 1 to the pagesCount because
	 * this method will not include the final number
	 * to our new array.
	 * For example, in this case we have 4 pages,
	 * so the method will create an array [1,2,3]
	 * if we add 1 to the pages we will get the
	 * desired size.
	 */
	const pages = _.range(1, pagesCount + 1);
	return (
		<nav aria-label='Page navigation example'>
			<ul className='pagination'>
				{pages.map((page) => (
					<li
						key={page}
						className={page === currentPage ? 'page-item active' : 'page-item'}
					>
						<a className='page-link' onClick={() => onPageChange(page)}>
							{page}
						</a>
					</li>
				))}
			</ul>
		</nav>
	);
};
/**
 * In order to keep things from breaking
 * we make sure we recieve the props
 * we need and they are the right time
 * of prop.
 */
Pagination.propTypes = {
	itemsCount: PropTypes.number.isRequired,
	pageSize: PropTypes.number.isRequired,
	currentPage: PropTypes.number.isRequired,
	onPageChange: PropTypes.func.isRequired,
};

export default Pagination;
