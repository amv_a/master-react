import React, { Component } from 'react';
import Input from './input';
import Select from './select';
import Joi from 'joi-browser';

class Form extends Component {
	state = {
		data: {},
		errors: {},
	};

	/**
	 * This method will validate the whole
	 * form on submit using a Joi schema and
	 * Joi functionalities
	 * @return {Object} erros: Any and all validation
	 * errors.
	 */
	validate = () => {
		const options = { abortEarly: false }; //will keep validating util the last input even if there is a validation issue.
		const { error } = Joi.validate(this.state.data, this.schema, options);
		if (!error) return null;
		const errors = {};
		for (let item of error.details) errors[item.path[0]] = item.message;
		return errors;
	};
	/**
	 * This method will validate a single input
	 * field when the user leaves its focus.
	 * @param {Stirng} name: The name of the input
	 * field.
	 * @param {String} value: The contents of the
	 * input field.
	 * @return {String} The error message if it exist
	 * if not it will return null.
	 */
	validateProperty = ({ name, value }) => {
		//Because we cannot use the full schema to validate
		//an elemenent, we make the params into an object
		//and extract the part of the Joi schema
		const obj = { [name]: value };
		const schema = { [name]: this.schema[name] };
		const { error } = Joi.validate(obj, schema);
		return error ? error.details[0].message : null;
	};

	/**
	 * This method will handle the submission of the form.
	 * It will also store the errors if there are some.
	 * @param {event} e: The onSubmit event.
	 */
	handleSubmit = (e) => {
		e.preventDefault();
		const errors = this.validate();
		this.setState({ errors: errors || {} });
		if (errors) return;
		this.doSubmit();
	};

	/**
	 * This method will handle the change
	 * on the input fields.
	 * @param {Component} Input: The input
	 * component being targeted.
	 */
	handleChange = ({ currentTarget: input }) => {
		//We change the name of the property to make it cleaner.
		const errors = { ...this.state.errors };
		const errorMessage = this.validateProperty(input);
		if (errorMessage) errors[input.name] = errorMessage;
		else delete errors[input.name];
		const data = { ...this.state.data };
		data[input.name] = input.value;
		this.setState({ data, errors });
	};
	renderButton(label) {
		return (
			<button disabled={this.validate()} className='btn btn-primary'>
				{label}
			</button>
		);
	}
	renderInput(name, label, type = 'text') {
		const { data, errors } = this.state;
		return (
			<div className='form-group'>
				<Input
					type={type}
					name={name}
					label={label}
					value={data[name]}
					error={errors[name]}
					onChange={this.handleChange}
				/>
			</div>
		);
	}

	renderSelect(name, label, options) {
		const { data, errors } = this.state;
		return (
			<Select
				name={name}
				label={label}
				value={data[name]}
				error={errors[name]}
				options={options}
				onChange={this.handleChange}
			/>
		);
	}
}

export default Form;
