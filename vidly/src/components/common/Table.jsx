import React from 'react';
import Table from 'react-bootstrap/Table';
import TableHeader from './tableHeader';
import TableBody from './tableBody';
/**
 * A functional component that puts together
 * the different parts of a table.
 * @param {Object} data
 * @param {Object} sortColumn
 * @param {Array} columns
 * @param {function} onSort
 */
const TableElement = ({ data, sortColumn, columns, onSort }) => {
	return (
		<Table striped bordered hover variant='dark' responsive>
			<TableHeader columns={columns} sortColumn={sortColumn} onSort={onSort} />
			<TableBody data={data} columns={columns} />
		</Table>
	);
};

export default TableElement;
