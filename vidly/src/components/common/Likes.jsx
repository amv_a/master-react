import React, { Component } from 'react';

/**
 * Class for a like icon component.
 */
class Likes extends Component {
	/**
	 * A method that changes the icon
	 * if the item is liked or not.
	 */
	changeClass() {
		let isLiked = this.props.like;
		let classes = 'fa fa-heart';
		if (!isLiked) classes += '-o';
		return classes;
	}
	render() {
		return (
			<i
				onClick={this.props.onClick}
				className={this.changeClass()}
				style={{ cursor: 'pointer' }}
				aria-hidden='true'
			/>
		);
	}
}

export default Likes;
