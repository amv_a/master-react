import React, { Component } from 'react';

/**
 * This custom component handles the heading
 * part of a table.
 * @param {Array} columns
 * @param {Object} sortColumn
 * @param {function} onSort
 */
class MoviesTable extends Component {
	/**
	 * A method that will modify the sortColumn
	 * object to display the desired ordenation.
	 * @param {String} path: The path of the column
	 * to be sorted.
	 */
	raiseSort = (path) => {
		const sortColumn = { ...this.props.sortColumn };
		if (sortColumn.path === path) {
			sortColumn.order = sortColumn.order === 'asc' ? 'desc' : 'asc';
		} else {
			sortColumn.path = path;
			sortColumn.order = 'asc';
		}
		this.props.onSort(sortColumn);
	};
	/**
	 * This method renders the arrow-like sort icon
	 * depending on the ordenation the sorting event
	 * will manage.
	 * @param {Object} column
	 * @return {icon html expresion}
	 */
	renderSortIcon = (column) => {
		const { sortColumn } = this.props;
		if (column.path !== sortColumn.path) return null;
		if (sortColumn.order === 'asc')
			return <i className='fa fa-sort-asc ml-2'></i>;
		return <i className='fa fa-sort-desc ml-2'></i>;
	};
	render() {
		return (
			<thead>
				<tr>
					{this.props.columns.map((column) => (
						<th
							className='clickable'
							key={column.path || column.key}
							onClick={() => this.raiseSort(column.path)}
						>
							{column.label}
							{this.renderSortIcon(column)}
						</th>
					))}
				</tr>
			</thead>
		);
	}
}

export default MoviesTable;
