import React, { Component } from 'react';
import Likes from './common/Likes';
import TableElement from './common/Table';
import { Link } from 'react-router-dom';
/**
 * Class for the table of the movies.
 */
class MoviesTable extends Component {
	/**
	 * We hold the info of the columns
	 * in order for them to be dinamically rendered.
	 */
	columns = [
		{
			path: 'title',
			label: 'Title',
			content: (movie) => (
				<Link to={`/movies/${movie._id}`}>{movie.title}</Link> //Routing link for the movieForm component.
			),
		},
		{ path: 'genre.name', label: 'Genre' },
		{ path: 'numberInStock', label: 'Number In Stock' },
		{ path: 'dailyRentalRate', label: 'Daily Rental Rate' },
		{
			key: 'like',
			content: (item) => (
				<Likes onClick={() => this.props.onLike(item)} like={item.liked} />
			),
		},
		{
			key: 'delete',
			content: (item) => (
				<button
					onClick={() => this.props.onDelete(item)}
					className='btn btn-danger'
				>
					Delete
				</button>
			),
		},
	];
	render() {
		const { movies, sortColumn, onSort } = this.props;
		return (
			<TableElement
				data={movies}
				columns={this.columns}
				sortColumn={sortColumn}
				onSort={onSort}
			/>
		);
	}
}

export default MoviesTable;
